import pymongo
from pymongo import MongoClient
from mongoengine import *
import datetime
import random
from pprint import pprint
import PracticaPokeMonGo2_Functions_Joshua as functions


#register_connection('project', 'default')

#client = MongoClient()
#db = client['project']
#coll = db['pokemon']

client = MongoClient()
db = connect('project')

TYPES = ("Bug","Dark","Dragon","Electric","Fairy","Fight","Fire","Flying","Ghost","Grass","Ground","Ice","Normal","Poison","Psychic","Rock","Steel","Water")
OBJECTS = ('Health','AttackX','DefenceX')

######################### PROGRAM START ####################

functions.startDB()
team = 'A' #Indica Torn

teamA, teamB = functions.getTeams()

pokemonA = teamA[0]
pokemonB = teamB[0]

buffAttackA = 0
buffAttackB = 0
buffDefenceA = 0
buffDefenceB = 0

teamCompA = ""
teamCompB = ""

items = list(functions.Items.objects())
itemList = ""

#print(pokemonA["name"])
#print(pokemonB["name"])

while(True):
    print("-------------------- Turno del equipo {} --------------------".format(team))

    fAttackA = functions.Moves.objects(id=pokemonA["moves"].get("fast")).get()
    cAttackA = functions.Moves.objects(id=pokemonA["moves"].get("charged")).get()

    fAttackB = functions.Moves.objects(id=pokemonB["moves"].get("fast")).get()
    cAttackB = functions.Moves.objects(id=pokemonB["moves"].get("charged")).get()

    pokemonA["attack"] += buffAttackA
    pokemonB["attack"] += buffAttackB

    pokemonA["defence"] += buffDefenceA
    pokemonB["defence"] += buffDefenceB

    itemList = teamCompA = teamCompB = ""

    for i in items:
        itemList += "{} | ".format(i["name"])

    for i in teamA:
        teamCompA += "{} / {} | ".format(i["name"], i["HP"])
    
    for i in teamB:
        teamCompB += "{} / {} | ".format(i["name"], i["HP"])

    print("EQUIPO A: " + teamCompA)
    print("EQUIPO B: " + teamCompB)
    print("")
    print("OBJETOS: " + itemList)
    print("")
    print("POKEMON ACTIVO A: Name: {} - HP: {} \t | Fast: {} - Charged: {} \t | Energia: {}".format(pokemonA["name"], pokemonA["HP"], fAttackA["name"], cAttackA["name"], pokemonA["energy"]))
    print("POKEMON ACTIVO B: Name: {} - HP: {} \t | Fast: {} - Charged: {} \t | Energia: {}".format(pokemonB["name"], pokemonB["HP"], fAttackB["name"], cAttackB["name"], pokemonB["energy"]))
    print("")

    while(True):
        consulta = input()
        partes = consulta.split(" ", 1)

        if(partes[0] == 'Attack'):
            if(partes[1] == 'Charged'):
                if(team == 'A'):
                    functions.chargedAttack(pokemonA, pokemonB, teamB)
                    if(pokemonB["HP"] <= 0):
                        if(teamB != []):
                            pokemonB = teamB[0]
                            buffAttackB = buffDefenceB = 0
                            
                else:
                    functions.chargedAttack(pokemonB, pokemonA, teamA)
                    if(pokemonA["HP"] <= 0):
                        if(teamA != []):
                            pokemonA = teamA[0]
                            buffAttackA = buffDefenceA = 0
                break;
            elif(partes[1] == 'Fast'):
                if(team == 'A'):
                    functions.fastAttack(pokemonA, pokemonB, teamB)
                    if(pokemonB["HP"] <= 0):
                        if(teamB != []):
                            pokemonB = teamB[0]
                            buffAttackB = buffDefenceB = 0
                            
                else:
                    functions.fastAttack(pokemonB, pokemonA, teamA)
                    if(pokemonA["HP"] <= 0):
                        if(teamA != []):
                            pokemonA = teamA[0]
                            buffAttackA = buffDefenceA = 0
                break;
            else:
                print("No existe la comanda...")

        elif(partes[0] == 'Change'):
            if(team == 'A'):
                pokemonA = functions.changePokemon(teamA, partes[1])
                if(pokemonA != None):
                    buffAttackA = buffDefenceA = 0
                    print("Entra el pokemon {} para el equipo A".format(pokemonA["name"]))
                    break;
                else:
                    print("El pokemon no existe en este equipo...")
            elif(team == 'B'):
                pokemonB = functions.changePokemon(teamB, partes[1])
                if(pokemonB != None):
                    buffAttackB = buffDefenceB = 0
                    print("Entra el pokemon {} para el equipo B".format(pokemonB["name"]))
                    break;
                else:
                    print("El pokemon no existe en este equipo...")

        elif(partes[0] == 'Use'):
            item = partes[1]
            
            if(team == 'A'):
                correct = functions.useItem(pokemonA, item, buffAttackA, buffDefenceA)

            else:
                correct = functions.useItem(pokemonB, item, buffAttackB, buffDefenceB)

            if(correct != True):
                print("El objeto no existe...")
            else:
                items = list(functions.Items.objects())
                break;
                    
        else:
            print("No existe la comanda...")

    if(len(teamA) == 0 or len(teamB) == 0):
        pprint("Acaba el combate! Gana el equipo {}".format(team))
        break
    
    if(team == 'A'):
        team = 'B'
    else:
        team = 'A'

    print("")

