import pymongo
from pymongo import MongoClient
from mongoengine import *
import datetime
import random
from pprint import pprint

#register_connection('project', 'default')

#client = MongoClient()
#db = client['project']
#coll = db['pokemon']

client = MongoClient()
db = client['project']

TYPES = ("Bug","Dark","Dragon","Electric","Fairy","Fight","Fire","Flying","Ghost","Grass","Ground","Ice","Normal","Poison","Psychic","Rock","Steel","Water")
OBJECTS = ('Health','AttackX','DefenceX')

########################## START CLASSES ##########################

    ########################### EVOLUTION ###########################
class Evolution(EmbeddedDocument):
    name = StringField(required = True)
    num = StringField(required = True)
    #type = StringField(required = True)

    ########################## CLASSES MOVES ##########################

class Moves(Document):
    meta = {'allow_inheritance': True}
    name = StringField(required = True)
    pwr = FloatField(required = True, min_value = 0, max_value = 180)
    type = StringField(required = True, choice = TYPES)

class FastMove(Moves):
    moveType = "Fast"
    energyGain = IntField(min_value = 0, max_value = 20)

class ChargedMove(Moves):
    moveType = "Charged"
    energyCost = IntField(min_value = 33, max_value = 100)

class Movimientos(EmbeddedDocument):
    fast = ReferenceField('Moves')
    charged = ReferenceField('Moves')

    ########################## ITEMS ##########################

class Items(Document):
    name = StringField(required=True)
    type = StringField(choices=OBJECTS,required=True)
    value = IntField(required=True)

    ########################## POKEMON TEAMS ##########################

class TeamA(Document):
    num = StringField(required = True)
    name = StringField(required = True)
    type = ListField(choice = TYPES, required = True)
    CP = FloatField(required = True)
    HPMax = FloatField(required = True,  min_value=200, max_value=1000)
    HP = FloatField(required = True, min_value=0, max_value=1000)
    attack = IntField(required = True, min_value = 10, max_value = 50)
    defence = IntField(required = True, min_value = 10, max_value = 50)
    energy = IntField(required = True, min_value = 0, max_value = 100)
    moves = EmbeddedDocumentField('Movimientos', required = True)
    candy = StringField(required = True)
    candy_count = FloatField(required = True)
    current_candy = FloatField(required = True)
    weaknesses = ListField(StringField(), required = True)
    next_evolution = ListField(EmbeddedDocumentField('Evolution'))

class TeamB(Document):
    num = StringField(required = True)
    name = StringField(required = True)
    type = ListField(choice = TYPES, required = True)
    CP = FloatField(required = True)
    HPMax = FloatField(required = True,  min_value=200, max_value=1000)
    HP = FloatField(required = True, min_value=0, max_value=1000)
    attack = IntField(required = True, min_value = 10, max_value = 50)
    defence = IntField(required = True, min_value = 10, max_value = 50)
    energy = IntField(required = True, min_value = 0, max_value = 100)
    moves = EmbeddedDocumentField('Movimientos', required = True)
    candy = StringField(required = True)
    candy_count = FloatField(required = True)
    current_candy = FloatField(required = True)
    weaknesses = ListField(StringField(), required = True)
    next_evolution = ListField(EmbeddedDocumentField('Evolution'))

########################## END CLASSES ##########################

########################### START FUNCTIONS ###########################

    ########################### moves ###########################

def generateMoves():
    fast_move1 = FastMove(name='Razor Leaf',pwr=13.0,type="Grass",energyGain=7)
    fast_move2 = FastMove(name='Steel Wing',pwr=11.0,type="steel",energyGain=6)
    fast_move3 = FastMove(name='Rock Throw',pwr=12.0,type="Rock",energyGain=7)
    fast_move4 = FastMove(name='Shadow Claw',pwr=9.0,type="Ghost",energyGain=8)
    fast_move5 = FastMove(name='Water Fall',pwr=16.0,type="Water",energyGain=8)
    fast_move6 = FastMove(name='Fire Spin',pwr=14.0,type="Fire",energyGain=10)
    fast_move7 = FastMove(name='Pound',pwr=7.0,type="Normal",energyGain=6)

    fMoves = [fast_move1,fast_move2,fast_move3,fast_move4,fast_move5,fast_move6,fast_move7]

    fast_move1.save()
    fast_move2.save()
    fast_move3.save()
    fast_move4.save()
    fast_move5.save()
    fast_move6.save()
    fast_move7.save()

    charge_move1 = ChargedMove(name='Ice Beam',pwr=130.0,type="Ice",energyCost=100)
    charge_move2 = ChargedMove(name='Hurricane',pwr=110.0,type="Flyng",energyCost=100)
    charge_move3 = ChargedMove(name='Outrange',pwr=110.0,type="Dragon",energyCost=50)
    charge_move4 = ChargedMove(name='Overheat',pwr=160.0,type="Fire",energyCost=100)
    charge_move5 = ChargedMove(name='Surf',pwr=65.0,type="Water",energyCost=50)
    charge_move6 = ChargedMove(name='Sludge',pwr=50.0,type="Poison",energyCost=33)
    charge_move7 = ChargedMove(name='Dig',pwr=100.0,type="Ground",energyCost=50)
    charge_move8 = ChargedMove(name='Swift',pwr=60.0,type="Normal",energyCost=50)

    cMoves = [charge_move1,charge_move2,charge_move3,charge_move4,charge_move5,charge_move6,charge_move7,charge_move8]

    charge_move1.save()
    charge_move2.save()
    charge_move3.save()
    charge_move4.save()
    charge_move5.save()
    charge_move6.save()
    charge_move7.save()
    charge_move8.save()

    return fMoves, cMoves

    ########################### generatePokemon ###########################

def generatePokemon(filter, team, fMoves, cMoves):
    HPmax= random.randint(200,1000)
    atk=random.randint(10,50)
    df=random.randint(10,50)
    CP=HPmax+atk+df

    db = client["project"]
    consulta = db["pokemon"].find_one({"num" : filter})

    #pprint(consulta)

    levol = []
    candy_c = 0

    if(consulta.get("candy_count") != None):
        candy_c = consulta.get("candy_count")

    if(consulta.get("next_evolution") != None):
        for dick in consulta.get("next_evolution"):
            levol.append(Evolution(name=dick["name"],num=dick["num"]))

    selectedMoves = Movimientos(fast = random.choice(fMoves), charged = random.choice(cMoves))

    if(team == "TeamA"):
        Pokemon = TeamA(
            num = consulta.get('num'), 
            name = consulta.get('name'), 
            type = consulta.get('type'),
            CP = CP, 
            HPMax = HPmax,
            HP = HPmax,
            moves = selectedMoves,
            attack = atk, 
            defence = df, 
            energy = 0,
            candy = consulta.get('candy'),
            candy_count = candy_c,
            current_candy = 0,
            weaknesses = consulta.get('weaknesses'),
            next_evolution = levol
        )

        Pokemon.save()

    else:
        Pokemon = TeamB(
            num = consulta.get('num'), 
            name = consulta.get('name'), 
            type = consulta.get('type'),
            CP = CP, 
            HPMax = HPmax,
            HP = HPmax,
            moves = selectedMoves,
            attack = atk, 
            defence = df, 
            energy = 0,
            candy = consulta.get('candy'),
            candy_count = candy_c,
            current_candy = 0,
            weaknesses = consulta.get('weaknesses'),
            next_evolution = levol
        )

        Pokemon.save()

    ########################### generateTeams ###########################
def generateTeams(fMoves, cMoves):
    pokemons = random.sample(range(1,151),12)

    for i in range(6):
        pokemon = "{0:03}".format(pokemons[i])
        #pprint(pokemon)
        generatePokemon(pokemon, 'TeamA', fMoves, cMoves)

    for i in range(6):
        pokemon = "{0:03}".format(pokemons[i+6])
        #pprint(pokemon)
        generatePokemon(pokemon, 'TeamB', fMoves, cMoves)

    ########################### generateItems ###########################
def generateItems():
    potion = Items(name="Potion",type="Health",value=20)
    superPotion1 = Items(name="Super Potion",type="Health",value=60)
    superPotion2 = Items(name="Super Potion",type="Health",value=60)
    hyperPotion1 = Items(name="Hyper Potion",type="Health",value=120)
    hyperPotion2 = Items(name="Hyper Potion",type="Health",value=120)

    xDefence = Items(name="X Defence",type="DefenceX",value=25)
    xAttack = Items(name="X Attack",type="AttackX",value=25)

    potion.save()
    superPotion1.save()
    superPotion2.save()
    hyperPotion1.save()
    hyperPotion2.save()
    xDefence.save()
    xAttack.save()

    ########################### startDB ###########################
def startDB():
    Moves.drop_collection()
    TeamA.drop_collection()
    TeamB.drop_collection()
    Items.drop_collection()

    fMoves, cMoves = generateMoves()
    generateTeams(fMoves,cMoves)
    generateItems()

    ########################### getTeams ###########################
def getTeams():
    teamA = list(db['team_a'].find({}))
    teamB = list(db['team_b'].find({}))
    return teamA, teamB

    ########################### chargedAttack ###########################
def chargedAttack(attacker, defender, team):
    attack = Moves.objects(id=attacker["moves"].get("charged")).get()

    if(attacker["energy"] >= attack["energyCost"]):
        if(attack['type'] in attacker['type']): stab = 1.5
        else: stab = 1

        if(attack['type'] in defender['type']): weak = 2
        else: weak = 1
        
        damage = int((attacker['attack'] * attack['pwr'] * weak * stab)/(defender['defence'] * 2))
        defender['HP'] -= damage

        attacker['energy'] -= attack['energyCost']

        if(defender['HP'] <= 0):
            print('El pokemon {} ha sido debilitado!'.format(defender['name']))
            
            team.remove(defender)
        else:
            print('El pokemon {} esta a {}!'.format(defender['name'], defender['HP']))

    else:
        print("El ataque falla!")

    ########################### fastAttack ###########################
def fastAttack(attacker, defender, team):
    attack = Moves.objects(id=attacker["moves"].get("fast")).get()

    if(attack['type'] in attacker['type']): stab = 1.5
    else: stab = 1

    if(attack['type'] in defender['type']): weak = 2
    else: weak = 1
    
    damage = int((attacker['attack'] * attack['pwr'] * weak * stab)/(defender['defence'] * 2))
    defender['HP'] -= damage

    attacker['energy'] += attack['energyGain']

    if(defender['HP'] <= 0):
        print('El pokemon {} ha sido debilitado!'.format(defender['name']))
        
        team.remove(defender)
    else:
        print('El pokemon {} esta a {}!'.format(defender['name'], defender['HP']))
    
    ########################### changePokemon ###########################
def changePokemon(team, text):
    pokemon = None
    for i in team:
        if(i["name"] == text):
            pokemon = i
            break

    return pokemon

    ########################### useItem ###########################
def useItem(pokemon, obj, buffAttack, buffDefence):
    try:
        item = Items.objects(name=obj)[0]
    except IndexError:
        item = None

    if(item != None):
        if(item["name"] == "X Attack"):
            buffAttack += item["value"]
        elif(item["name"] == "X Defense"):
            buffDefence += item["value"]
        elif(item["type"] == "Health"):
            if(pokemon["HP"] + item["value"] > pokemon["HPMax"]):
                pokemon["HP"] = pokemon["HPMax"]
            else:
                pokemon["HP"] += item["value"]

        item.delete()
        return True
    else:
        return False
    
########################### END FUNCTIONS ###########################
