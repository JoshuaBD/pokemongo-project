import pymongo
from pymongo import MongoClient
import datetime
import random

client = MongoClient()
#print(client.database_names())

#--------------------------------------------------------------------------------------------------------------------------------------------

def search(nombre, wd9minu): #El nombre lo ha puesto Juanjo, dandole una ostia a mi teclado. Se queda asi en honor a las teclas caidas
    
    db = client["project"]
    coll = db["pokemon"]

    evol = False
    printing = True

    if len(wd9minu) == 1 and wd9minu[0] == "evol":
        evol = True
        
        project = {}
        project["_id"] = 0
        project["name"] = 1
        project["next_evolution"] = 1
        project["prev_evolution"] = 1
        
        #print(project)

    elif "evol" not in wd9minu:
        project = {}

        project["_id"] = 0
        project["name"] = 1

        for i in wd9minu:
            project[i] = 1

        #print(project)

    else:
        printing = False

    if printing == True:
        resultat = coll.find_one({"name" : nombre}, project)
        if resultat == None:
            resultat = coll.find_one({"num" : nombre}, project)

        mostrarResultat = ()

        if resultat != None:
            if evol == False:
                for i in resultat.values():
                    mostrarResultat += (i,)
            else:
                for i in resultat.values():
                    if isinstance (i,str):
                        mostrarResultat += (i,)
                    else:
                        for j in i:
                            mostrarResultat += (j.get('name'),)

            print(mostrarResultat)

        else:
            print("No es pot realitzar la consulta")
    else:
        print("No es pot fer altres operacions amb EVOL")
        #coll.find({"name" : nombre}).projection("")

#--------------------------------------------------------------------------------------------------------------------------------------------

def catch(filter):
    db = client["project"]
    coll = db["team"]
    
    consulta = coll.find_one({"name" : filter})
    if consulta == None:
        consulta = coll.find_one({"num" : filter})

    #print(consulta)

    if consulta == None:
        consulta = db["pokemon"].find_one({"name" : filter}) #Busco que exista el pokemon y solo insertamos en el equipos si existe
        if consulta == None:
            consulta = db["pokemon"].find_one({"num" : filter})
        
        if consulta != None:
            CP = random.randint(0,500)
            coll.insert_one({"num" : consulta.get('num'), "name" : consulta.get('name'), "catch_date" : datetime.datetime.now(), "CP" : CP, "candy" : consulta.get("candy"), "candy_count" : consulta.get("candy_count"), "current_candy" : 0, "next_evolution" : consulta.get("next_evolution")})
            s = "Has atrapat {}. Tens {} al teu equip".format(filter, coll.count_documents({}))
            print(s)
        else:
            print("Aquest Pokemon no existeix...")            
    else:
        print("Ja tens el Pokemon " + filter)

#--------------------------------------------------------------------------------------------------------------------------------------------

def release(filter):
    db = client["project"]
    coll = db["team"]

    consulta = coll.find_one({"name" : filter})
    if consulta == None:
        consulta = coll.find_one({"num" : filter})

    if consulta != None:
        coll.delete_one({"name" : consulta.get("name")})

        s = "Has alliberat el pokemon {}. Tens {} al teu equip".format(filter, coll.count_documents({}))
        print(s)
    else:
        print("No tens o no existeix el Pokemon " + filter)


#--------------------------------------------------------------------------------------------------------------------------------------------

def candy(filter):
    db = client["project"]
    coll = db["team"]

    consulta = coll.find_one({"name" : filter})
    if consulta == None:
        consulta = coll.find_one({"num" : filter})

    if consulta != None and consulta.get("candy_count") != None:
        if consulta.get("current_candy")+1 < consulta.get("candy_count"):
            coll.update_one({"name" : consulta.get("name")}, {'$set': {"current_candy" : consulta.get("current_candy")+1}})
            s = "Has donat un caramel al pokemon {}. Té {} caramels".format(filter, consulta.get("current_candy")+1)
            print(s)
        else:
            evolucio = db["pokemon"].find_one({"name" : consulta.get("next_evolution")[0].get("name")})

            #print(evolucio)

            if evolucio.get("next_evolution") == None:
                coll.update_one({"name" : consulta.get("name")}, {'$set': {"num" : evolucio.get("num"), "name" : evolucio.get("name"), "CP" : consulta.get("CP")+100}, '$unset' : {"current_candy" : "", "next_evolution" : "", "candy_count" : ""}})
            else:
                coll.update_one({"name" : consulta.get("name")}, {'$set': {"num" : evolucio.get("num"), "name" : evolucio.get("name"), "CP" : consulta.get("CP")+100, "candy_count" : evolucio.get("candy_count"), "current_candy" : 0, "next_evolution" : evolucio.get("next_evolution")}})
            
            s = "Has donat un caramel al pokemon {}. Ha evolucionat a {}!".format(filter, evolucio.get("name"))
            print(s)
    else:
        print("No tens, no existeix o no té evolució el Pokemon " + filter)

#--------------------------------------------------------------------------------------------------------------------------------------------

def types(filter):
    db = client["project"]
    coll = db["pokemon"]

    consulta = coll.aggregate([{'$unwind' : "$type"}, {'$match' : {'type' : filter}}, {'$project' : {'_id' : 0, 'name' : 1}}])

    lista = []

    for c in consulta:
        lista.append(c["name"])

    if len(lista) > 0:
        s = "{} {}".format(lista, len(lista))
        print(s)
    else:
        print("El TYPE introduit no existeix....")

    

#--------------------------------------------------------------------------------------------------------------------------------------------

while True:
    consulta = input()

    partes = consulta.split()

    if partes[0] == "Exit":
        break

    elif partes[0] == "Search":
        x = []

        for i in partes[2 : len(partes)]:
            x.append(i)

        search(partes[1], x)

    elif partes[0] == "Catch":
        catch(partes[1])

    elif partes[0] == "Release":
        release(partes[1])

    elif partes[0] == "Candy":
        candy(partes[1])
    
    elif partes[0] == "Type":
        types(partes[1])

    else:
        print("Digues una comanda...")



